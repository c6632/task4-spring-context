package ru.csu.task4springcontext;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.csu.task4springcontext.service.HelloService;

@SpringBootApplication
public class Task4SpringContextApplication {

	public static void main(String[] args) {
		var context = SpringApplication.run(Task4SpringContextApplication.class, args);
		var service = context.getBean(HelloService.class);
		service.sayHello();
	}

}
