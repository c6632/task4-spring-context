package ru.csu.task4springcontext.service.impl;

import org.springframework.stereotype.Service;
import ru.csu.task4springcontext.service.HelloService;
import ru.csu.task4springcontext.service.MessageRenderer;

@Service
public class HelloServiceImpl implements HelloService {
    private final MessageRenderer renderer;


    public HelloServiceImpl(MessageRenderer renderer) {
        this.renderer = renderer;
    }


    @Override
    public void sayHello() {
        renderer.render("I am say Hello World");
    }
}
