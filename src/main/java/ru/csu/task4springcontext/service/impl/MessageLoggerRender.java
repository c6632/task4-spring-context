package ru.csu.task4springcontext.service.impl;

import lombok.extern.log4j.Log4j2;
import ru.csu.task4springcontext.service.MessageRenderer;

@Log4j2
public class MessageLoggerRender implements MessageRenderer {

    public MessageLoggerRender() {
        System.out.println();
    }

    @Override
    public void render(String message) {
        log.info(message);
    }
}
